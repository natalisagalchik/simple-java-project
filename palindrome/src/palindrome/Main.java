package palindrome;

public class Main {

    public static void main(String[] args) {
        String str = "abba";
        printOut(isPalindrome(str));

    }

    private static void printOut(boolean str) {
        System.out.println(str);
    }


    public static boolean isPalindrome(String str) {
        if (str == null || str.isEmpty()) return false;

        char[] arrayCh = str.toCharArray();
        int arrLenght = arrayCh.length;
        for (int i = 0; i < arrLenght / 2; i++) {
            if (arrayCh[i] != arrayCh[arrLenght - i - 1]) {
                return false;
            }
        }
        return true;
    }
}



